from typing import Text
from generic_file import *

class Dokument(GenerickiDokument):
    def __init__(self, name: Text):  # name u formatu naziv.ekstenzija
        super().__init__()
        self.pages = []
        txt = name.split(".")
        self.name = txt[0]  # naziv
        self.type = txt[1]  # extenzija

    def sacuvaj(self):
        if self.type == "txt":
            content = ""
            for page in self.pages:
                for slot in page.slots:
                    for element in slot.elements:
                        content += element.content
                    content += "\n"
                content += "\n\n"
                #sacuvaj txt fajl   
        if self.type == ".pptx":
            pass  # https://python-pptx.readthedocs.io/en/latest/user/quickstart.html

    
