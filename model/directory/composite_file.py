from abc import ABC
from generic_file import *

class KompozitniDokument(GenerickiDokument, ABC):
    def __init__(self):
        super().__init__()
        self.generic_files = []