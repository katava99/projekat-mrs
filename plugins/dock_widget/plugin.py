from plugin_framework.extension import Extension
from .dockWidget import DockWidget

class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.dock = DockWidget(iface)# ovde pozivamo dock widget a iface.dock_widget je iz mainwindow, 
                                                    #u mainwindow imamo metodu add_dock_widget preko nje dodajemno ovaj dockwidget glavni prozor

        print("INIT TEST")
    
    # FIXME: implementacija apstraktnih metoda
    def activate(self):
        print("Dock widget plugin Activated")
        self.iface.add_dock_widget(self.dock)


    def deactivate(self):
        print("Deactivated")
        self.iface.remove_widget(self.dock)
