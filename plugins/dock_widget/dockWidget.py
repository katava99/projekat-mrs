from enum import IntEnum
from PySide2 import QtWidgets, QtCore, QtGui
from PySide2.QtWidgets import QMessageBox
import os
from PySide2.QtWidgets import*
from PySide2.QtGui import*
from PySide2 import QtCore

class DockWidget(QtWidgets.QWidget):
    widget_for = 1234
    def __init__(self, iface):
        super().__init__(iface)

        self.iface = iface
        
        self.layout = QtWidgets.QVBoxLayout()
        self.model = QtWidgets.QFileSystemModel()
        self.model.setRootPath(QtCore.QDir.rootPath())

        self.tree = QtWidgets.QTreeView()
        self.tree.setModel(self.model)
        self.tree.setRootIndex(self.model.index(QtCore.QDir.currentPath()))
        self.tree.clicked.connect(self.Slika)

        self.layout.addWidget(self.tree)
        self.setLayout(self.layout)
        

    def Slika(self, index):
        ext = [".jpg", ".png", ".pdf"]
        indexItem = self.model.index(index.row(), 0, index.parent())

        file = self.model.filePath(indexItem)
        if file.endswith(tuple(ext)):
            
                index = self.tree.currentIndex()
                file_path = self.model.filePath(index)

                fileName = QtCore.QFileInfo(file).fileName()
            
                self.picture = QPixmap(file_path)
                self.label = QLabel(self)
                self.label.setPixmap(self.picture)
                self.label.setFixedSize(500, 500)
                self.label.setAlignment(Qt.AlignCenter)
                self.iface.central_widget.addTab(self.label, QtGui.QIcon("resources/icons/Quit.png"), fileName)
            
        else:
            
                    try:
                        with open(file ,'r') as f:
                            tekst = f.read()
                    
                        fileName = QtCore.QFileInfo(file).fileName()

                        self.text_edit= QtWidgets.QTextEdit(tekst)
                        self.iface.central_widget.addTab(self.text_edit, QtGui.QIcon("resources/icons/Quit.png"), fileName)

                    except UnicodeDecodeError as type_error:
                        QMessageBox.information(self, "Message", "Mozete otvoriti samo tekstualne fajlove i slike.")
