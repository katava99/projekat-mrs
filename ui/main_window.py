from PySide2 import QtWidgets, QtGui, QtCore
from PySide2.QtCore import QDir
from PySide2.QtWidgets import QFileDialog
from PySide2.QtWidgets import QLabel
from PySide2.QtWidgets import QMessageBox


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, title, icon, parent=None):
        super().__init__(parent)                        #da li ima neki wgt od koga zavisi
        self.setWindowTitle(title)
        self.setWindowIcon(icon)
        self.resize(1200, 720)                            #mozemo staviti da bude i full screen

        #meni
        self.menu_bar = QtWidgets.QMenuBar(self)
        #toolbar
        self.tool_bar = QtWidgets.QToolBar("Toolbar", self)
        #self.tool_bar.setToolButtonStyle()                   #da primenimo razne stvari za button u tool-bar-u
        
        #statusbar
        self.status_bar = QtWidgets.QStatusBar(self)
        #self.widget = QtWidgets.QWidget(self)
        #centralwidget
        self.central_widget = QtWidgets.QTabWidget(self)
        self.central_widget.setTabsClosable(True)
        self.central_widget.tabCloseRequested.connect(self.delete_tab)
        #dockwidget
        self.dock_widget = QtWidgets.QDockWidget(self)

        #self.tree = QtWidgets.QTreeView(self.dock_widget)
        
        self.dock_widget.setWindowTitle("All files")

        #self.central_widget.addWidget(self.text_edit)


        #widgets in stacked widget
        self.widgets = {}                                                       #recnik za wgt-e koji se dodaju u centralWgt tj. na stek

        self.document = None  #otvoreni dokument (trenutno aktivni dokument)

        self.actions_dict = {
            # FIXME: ispraviti ikonicu na X
            "quit": QtWidgets.QAction(QtGui.QIcon("resources/icons/Quit.png"), "&Quit", self),                               #ovo sa &znaci da kad pritisnemo alt + prvo slovo od ovoga Quit
            "newFile": QtWidgets.QAction(QtGui.QIcon("resources/icons/newFile.png"), "&New File", self),
            "openFile": QtWidgets.QAction(QtGui.QIcon("resources/icons/openFile2.png"), "&Open File", self),
            "newWorkspace": QtWidgets.QAction(QtGui.QIcon("resources/icons/openFile2.png"), "&New Workspace", self),
            "newFolder": QtWidgets.QAction(QtGui.QIcon("resources/icons/openFile2.png"), "&New Folder", self),

            "save": QtWidgets.QAction(QtGui.QIcon("resources/icons/savefile.jpg"), "&Save", self),
            "saveAs": QtWidgets.QAction(QtGui.QIcon("resources/icons/saveAs.png"), "&Save As", self),
            "numberOfPage": QtWidgets.QAction("&Insert number page", self),
            "closeDocument": QtWidgets.QAction(QtGui.QIcon("resources/icons/closeDoc.png"), "&Close doc", self),
            "nextPage": QtWidgets.QAction(QtGui.QIcon("resources/icons/nextPage.jpg"), "&Next page", self),
            "previousPAge": QtWidgets.QAction(QtGui.QIcon("resources/icons/prevPage.jpg"), "&Prev page", self),
            "undo": QtWidgets.QAction(QtGui.QIcon("resources/icons/undo.png"), "&Undo", self),
            "redo": QtWidgets.QAction(QtGui.QIcon("resources/icons/redo.png"), "&Redo", self),
            "insertNewEmptyPage": QtWidgets.QAction(QtGui.QIcon("resources/icons/insertPage.jpg"), "&New page", self),
            "deletePage": QtWidgets.QAction(QtGui.QIcon("resources/icons/deletePage.png"), "&Delete page", self),
            "closePage": QtWidgets.QAction(QtGui.QIcon("resources/icons/closePage.jpg"), "&Close page", self),
            "copy": QtWidgets.QAction("&Copy", self),
            "paste": QtWidgets.QAction("&Paste", self),
            "cut": QtWidgets.QAction("&Cut", self),
            "about": QtWidgets.QAction("&About", self),
            "plugins": QtWidgets.QAction("&Plugins", self)


            # TODO: dodati i ostale akcije za help i za npr. osnovno za dokument
            # dodati open...
        }

        self._bind_actions()                                    #metoda koja omogucava da se setuje shortcut i da se setuje koja funkcija i metoda se poziva nakon sto se trigeruje data akcija 

        self._populate_menu_bar()

        self._populate_status_bar()
        self.get_broj_stranice()
        self.get_autor()

        self._populate_tool_bar()

        self.setMenuBar(self.menu_bar)                          #sve sto wgt moze da ima samo jedno pocinje sa set, sve sto moze da ima vise pocinje sa add
        self.addToolBar(self.tool_bar)

        self.setStatusBar(self.status_bar)

        self.setCentralWidget(self.central_widget)

    def _populate_menu_bar(self):
        file_menu = QtWidgets.QMenu("&File")
        help_menu = QtWidgets.QMenu("&Help")
        edit_menu = QtWidgets.QMenu("&Edit")
        
        file_menu.addAction(self.actions_dict["newFile"])
        file_menu.addAction(self.actions_dict["openFile"])
        file_menu.addAction(self.actions_dict["save"])
        file_menu.addAction(self.actions_dict["saveAs"])
        file_menu.addAction(self.actions_dict["plugins"])
        file_menu.addSeparator()
        file_menu.addAction(self.actions_dict["quit"])
        self.menu_bar.addMenu(file_menu)

        edit_menu.addAction(self.actions_dict["undo"])
        edit_menu.addAction(self.actions_dict["redo"])
        edit_menu.addSeparator()
        edit_menu.addAction(self.actions_dict["copy"])
        edit_menu.addAction(self.actions_dict["paste"])
        edit_menu.addAction(self.actions_dict["cut"])
        self.menu_bar.addMenu(edit_menu)

        help_menu.addAction(self.actions_dict["about"])
        self.menu_bar.addMenu(help_menu)



    def _populate_status_bar(self):
        self.wc_label = QLabel(self.get_broj_stranice())
        self.wc_label2 = QLabel(self.get_autor())
        self.status_bar.addPermanentWidget(self.wc_label)
        self.status_bar.addPermanentWidget(self.wc_label2)


    def get_autor(self):
        return "Author: User"

    def get_broj_stranice(self):
        return "Page number: 1"      

        

    def add_widget(self, widget):
        """
        Adds widget to central (stack) widget
        """
        self.widgets[widget.widget_for] = self.central_widget.currentIndex()
        self.central_widget.addWidget(widget)

                                                                                             #QMenuItem mozemo da dodajemo pored akcija u meni

                                                                                               #mozemo da imamo i separator u menijima, 

    def _bind_actions(self):
        self.actions_dict["quit"].setShortcut("Ctrl+Q")
        self.actions_dict["quit"].triggered.connect(self.close)                         #kad se trigeruje akcija, prosledjuje se metoda bez () zato sto ne poziva metoda nego se prosledjuje putanja do nje
        
        self.actions_dict["openFile"].setShortcut("Ctrl+O")
        self.actions_dict["openFile"].triggered.connect(self.otvoriPostojeciFajl)

        self.actions_dict["newWorkspace"].setShortcut("Ctrl+W")
        self.actions_dict["newWorkspace"].triggered.connect(self.napraviNoviRadniProstor)

        self.actions_dict["newFolder"].setShortcut("Ctrl+F")
        self.actions_dict["newFolder"].triggered.connect(self.napraviNoviDirektorijum)

        self.actions_dict["newFile"].setShortcut("Ctrl+N")
        self.actions_dict["newFile"].triggered.connect(self.napraviNoviFajl)

                                                                          
        self.actions_dict["saveAs"].triggered.connect(self.sacuvajKao)

        self.actions_dict["save"].setShortcut("Ctrl+S")      

        self.actions_dict["copy"].setShortcut("Ctrl+C")    

        self.actions_dict["paste"].setShortcut("Ctrl+V")   

        self.actions_dict["cut"].setShortcut("Ctrl+X")     

        self.actions_dict["undo"].setShortcut("Ctrl+Z")

        self.actions_dict["redo"].setShortcut("Ctrl+Y")




    def _populate_tool_bar(self):
        self.tool_bar.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.tool_bar.addAction(self.actions_dict["newWorkspace"])
        self.tool_bar.addAction(self.actions_dict["newFolder"])
        self.tool_bar.addAction(self.actions_dict["newFile"])       #ovde cemo dodati sve akcije koje imamo
        self.tool_bar.addAction(self.actions_dict["openFile"])
        self.tool_bar.addAction(self.actions_dict["save"])
        self.tool_bar.addAction(self.actions_dict["saveAs"])

        self.tool_bar.addSeparator()

        line_edit = QtWidgets.QLineEdit(self)              #u tool bar je dodat wgt i na njega je dodata akcija
        line_edit.setPlaceholderText("Unesite broj stranice za pretragu")
        line_edit.setValidator(QtGui.QIntValidator(0, 1000000, self))            #moguce je da se unese minimum 0 a maksimum 1000000
        line_edit.addAction(self.actions_dict["numberOfPage"])
        self.tool_bar.addWidget(line_edit)

        self.tool_bar.addSeparator()

        self.tool_bar.addAction(self.actions_dict["closeDocument"])
        self.tool_bar.addAction(self.actions_dict["previousPAge"])
        self.tool_bar.addAction(self.actions_dict["nextPage"])


    def napraviNoviRadniProstor(self):
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.Directory)
        dialog.setFilter(QDir.AllDirs)

        if dialog.exec_():
            file_name = dialog.selectedFiles()
            # TODO dodati fajl u stablo fajlova

        

    def napraviNoviDirektorijum(self):
        pass

    #ovo za sada radi sa dokumentima iz kojih mozemo da iscupamo tekst, znaci .txt, .py, itd. za pdf i pptx fajlove jos ne radi
    def otvoriPostojeciFajl(self):              #za pocetak neka bude implementirano samo da otvara dijalog
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.AnyFile)
        dialog.setFilter(QDir.Files)
        try:
            if dialog.exec_():
                file_name = dialog.selectedFiles()
            #if file_name[0].endswith(".txt"):        #ako zelimo da tipiziramo
                with open(file_name[0], "r") as f:
                    podaci = f.read()
                #self.central_widget.addWidget(QtWidgets.QTextEdit(podaci, self))
                                 #treba da stavim na stack wgt ovo, valjda?
            else:
                pass
        except UnicodeDecodeError as type_error:
            QMessageBox.information(self, "Open a new file", "Mozete otvoriti samo tekstualne fajlove.")


    def napraviNoviFajl(self):
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.AnyFile)
        dialog.setFilter(QDir.Files)

        if dialog.exec_():
            file_name = dialog.selectedFiles()
        
            #dalje bi smo pravili novi fajl
            #if file_name[0].endswith(".txt"):
        #     with open(file_name[0], "r") as f:
        #         podaci = f.read()
        #         #self.central_widget.addWidget(QtWidgets.QTextEdit(podaci, self))
        #         self.setCentralWidget(QtWidgets.QTextEdit(podaci, self))             #treba da stavim na stack wgt ovo
        # else:
        #     pass

    def sacuvajKao(self):
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.AnyFile)
        dialog.setFilter(QDir.Files)

        if dialog.exec_():
            file_name = dialog.selectedFiles()
        
            #dalje bi smo sacuvajli ovaj fajl kao novi
            #if file_name[0].endswith(".txt"):
        #     with open(file_name[0], "r") as f:
        #         podaci = f.read()
        #         #self.central_widget.addWidget(QtWidgets.QTextEdit(podaci, self))
        #         self.setCentralWidget(QtWidgets.QTextEdit(podaci, self))             #treba da stavim na stack wgt ovo
        # else:
        #     pass  



    def add_widget(self, widget):
        """
        Adds widget to central (stack) widget
        """
        self.widgets[widget.widget_for] = self.central_widget.currentIndex()
        self.central_widget.addWidget(widget)

    def remove_widget(self, widget):
        self.central_widget.removeWidget(widget)
    
    def add_dock_widget(self, widget):#metoda za dodavanje u widgeta, u main.py je plugin_registry i tamo se stavi taj id od vidzeta i poziva kao ostali
        self.widgets[widget.widget_for] = self.dock_widget.widget()
        self.dock_widget.setWidget(widget)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.dock_widget)

    def delete_tab(self,index):
        self.central_widget.removeTab(index)

    #def setText(self, text):
        #self.text_edit.setText(text)

    # TODO: dodati metodu koja ce u fokus staviti trenutko aktivni plugin (njegov widget)
